const jwt = require('jsonwebtoken');
const fs = require('fs');

app.put("/transaction", isAuthenticated ,(req, res) => {
    var userId = req.body.UserId;
    var amount = req.body.Amount;
    var coinName = req.body.CoinName;
    //Do the logic of updating the user currency and return one of the following:
    //  Success – no error, the operation finished successfully.
    //  UserNotExist – the user identifier does not exist
    //  CoinNameNotExist – the coin name does not exist
    //  InsufficientFunds – in case the action is reducing coins and the user doesn’t have enough.
    //  Other – the operation failed for a reason not listed above. 
});


app.get("/balance/:id/:coinName", isAuthenticated ,(req, res) => {
    var userId = req.params["id"];
    var coinName = req.params["coinName"];
    //return the current balance the user has
});


function isAuthenticated(req, res, next) {
    if (typeof req.headers.authorization !== "undefined") {
        var verifyOptions = {
            algorithm:  ["RS256"]
           };

        let token = req.headers.authorization.split(" ")[1];
 	    let publicKey = fs.readFileSync('./keys/ovioPublicKey.key', 'utf8');
        jwt.verify(token, publicKey, verifyOptions, (err, user) => {
            
            // if there has been an error...
            if (err) {  
                // shut them out!
                res.status(500).json({ error: "Not Authorized - bad token" });
                throw new Error("Not Authorized token");
            }
            return next();
        });
    } else {
        res.status(500).json({ error: "Not Authorized - no token" });
        throw new Error("Not Authorized - no token");
    }
}
