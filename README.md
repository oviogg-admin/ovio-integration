# OviO Integration Documentation README #

## Intro ##

Adding a game to OviO's platform requires a simple and flexible integration based on Two main API calls for functionality 
and one Deep Link for identification purposes between OviO and the Game.
In addition, we support an <b>optional</b> API to support Dynamic stores based on the User's Segment/configuration in the game.
A dedicated Bitbucket repository will be shared during the Preparation phase.

## Part 1 – API Calls ##

### Overview ###

To integrate with OviO, partners will need to provide Two main API calls for functionality. 
All API calls will be made over HTTPS and we will not make any calls 
over plain HTTP. Each API will have predictable and reasonable resource-oriented URLs, 
will accept, and return simple JSON-encoded bodies and responses, and uses standard 
HTTP response codes, authentication, and verbs. We can support different URLs for 
different regions or same URL for multiple regions

### Authentication ###

OviO will use OAuth2, access token (bearer auth) to authenticate. 
We will send a bearer token on each API request which will be signed with our private key which is secured and protected by AWS Key Management service.
OviO will supply the public key for verification and can also supply the function for that.
In addition, the “aud” claim will contain the Game Id, provided by OviO during the integration

### Errors ###

OviO uses conventional HTTP response codes to understand the success or failure of an API request. In general: Codes in the 2xx range indicate success. 
Codes in the 4xx range indicate an error that failed given the information provided (example: a required parameter was omitted, a charge failed, etc.). 
Codes in the 5xx range indicate an error with the game’s servers.

### Base url ###

OviO will require a base URL for all API calls (the base URL can change from region to region), provided by the partner during the integration

### API Calls ###

<b> Update User Currency API: </b>
This API call will add the amount of currency to the specified user (based on the User's identifier in the game - see 'Identification' section below).
This API call will be authenticated server to server as described above. 

The request:
A PUT request with a body that is comprised of the user unique identifier in the game, the amount to add and the currency (Coin) name[^1] that was provided by the partner. 
<br>
####
Endpoint: PUT {base url}/transaction<br>
Body: <br>
{<br>
	 &nbsp;&nbsp; UserId: string,<br>
	 &nbsp;&nbsp; Amount: int,<br>
	 &nbsp;&nbsp; CoinName: string<br>
}<br>
<br>
Returns: <br>
	{<br>
		Error: string<br>
}<br>
####

The error can be 1 of these options:

  - <b>Success</b> – no error, the operation finished successfully.<br>
  - <b>UserNotExist</b> – the user identifier does not exist<br>
  - <b>CoinNameNotExist</b> – the coin name does not exist<br>
  - <b>Other</b> – the operation failed for a reason not listed above. <br>

[^1]: OviO can support multiple currencies from the same game, we suggest you verify this parameter even if the game integrates only 1 currency type with OviO.

<b>Current Balance: </b>
This API call will return the user’s current game currency balance. 
This API call will be authenticated server to server as described above.
The request:
A GET request with 2 parameters (from URL) – parameter of the user unique identifier in the game and a parameter of the currency name (also described above).
<br>
####
Endpoint: GET {base url}/balance/<user id>/<coin name><br>

Returns:{<br>
		&nbsp;&nbsp; Amount: int<br>
}<br>

<b>Dynamic Store (Optional): </b>
This API call will return the user’s dynamic store values (prices, multipliers, etc). 
This API call will be authenticated server to server as described above.
The request:
A GET request with 2 parameters (from URL) – parameter of the user unique identifier in the game and a parameter of the currency name (also described above).
<br>
####
Endpoint: GET {base url}/dynamic/<user id>/<coin name><br>

Returns:{<br>
		&nbsp;&nbsp; Values: int[]<br>
}<br>
####

## Part 2 – Identification ##
### Overview ###
In order to use the APIs, OviO requires the user's identifier (UserId) in the game, in orde rto credit and track the specific user.
For that we require a simple identification process using deeplinks. 
We support 2 implementation methods for identification, the 1st is implementing Deeplinks on the Game's end and the 2nd is based on the Game's MMP (e.g. Appsflyer, Adjust) postback capabilities.


## Option 1: DeepLinks implemented in game: ##

### Overview ###
		
For User Identification purposes, you will need to implement a Deep Link mechanism.
The process will begin with OviO initiating the game’s Deep Link and in response, the game <br>
should initiate OviO’s deep link with the unique User Identifier and the Game Identifier as <br>
parameters. This will allow us to identify the source (game) of the User.<br>

### Deep Link structure ###

OviO’s Deep Link’ structure will be: oviogg://app/user/<game identifier>/<user id>
For example, “oviogg://app/user/com.example.mygame/2013141424214”.<br>
We recommend that the provided Deep Link will include an identification of OviO so it <br>
can be identified and have different logic from other/internal Deep Links (e.g. 
“mygame://ovio”).

### Implementation – Example in unity ###

<u>Android</u>: You will need to add the following data in the <application> tag to your android manifest 
file the schema (the URL for your app): 
 <data android:scheme="<URL Name for e.x: duels>" />
 
 <u>iOS</u>: Go to <b>Edit->Project Settings->iOS</b>
	  Select <b>Other Settings</b> and in the <b>Configuration</b> section add the deeplink schema to <b>Supported URL schemes</b>

Following this, you will need to create a game object in your unity for processing deep 
links, for example:

![Alt text](Game/Readme Images/gameObject.png?raw=true "Game Object")

Create a script and attach it to this game object.

## Option 2: Using your MMP: ##
### Overview ###
		
For User Identification purposes, you will need to create a custom MMP link (or links) for OviO.
The custom MMP link campaign will provide the user with a one time amount of in-game currency, which will be used by OviO for the user's first purchase.
In addition, we will add OviO's user idendtifier in the deep link value and in the game, the MMP link will need to call an event that will trigger a postback to 
OviO with both OviO's user id and the game's user id.

### Postback ###
The method for the postback will be POST.
The message field must include the user id in the game (for example - "Customer User Id" in AppsFlyer) and the OviO's user id in a sub param (for example - "Sub Param 1" in AppsFlyer).
The params can be changed per game - please contact OviO for that.

The endpoint url should be as follows:
https://prod.ovio.gg/api/vendor/{devId}/userIdentification

the {devId} will be provided by OviO to each partner and should never be shared.

**Be aware that choosing this option requires you to always keep a MMP link for OviO.